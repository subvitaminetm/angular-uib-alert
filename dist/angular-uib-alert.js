angular.module('angular-uib-alert', []).factory('$uibAlert', ['$q', '$uibModal', function ($q, $uibModal) {
        return {
            alert: function (message) {
                var q = $q.defer();
                // set template
                var template = '<div class="modal-body">';
                template += '<div class="text-center">';
                template += '<hr class="hr-empty hr-md" />';
                template += '<h4>{{message}}</h4>';
                template += '<hr class="hr-empty hr-md" />';
                template += '</div>';
                template += '</div>';
                template += '<div class="modal-footer">';
                template += '<button type="button" role="button" class="btn btn-primary" ng-click="close()">OK</button>';
                template += '</div> ';
                var modalInstance = $uibModal.open({
                    template: template,
                    controller: 'angularUibAlertCtrl',
                    windowClass: 'show',
                    size: 'sm',
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        message: function () {
                            return message;
                        }
                    }
                });
                modalInstance.result.then(function (response) {
                    q.resolve(response);
                }, function () {});
                return q.promise;
            },
            confirm: function (message, buttons) {
                var q = $q.defer();
                // set template
                var template = '<div class="modal-body">';
                template += '<div class="text-center">';
                template += '<hr class="hr-empty hr-md" />';
                template += '<h4>{{message}}</h4>';
                template += '<hr class="hr-empty hr-md" />';
                template += '</div>';
                template += '</div>';
                template += '<div class="modal-footer">';
                template += '<button type="button" role="button" class="{{button.class}}" ng-click="response(button.value)" ng-repeat="button in buttons">{{button.text}}</button>';
                template += '<div ng-if="!buttons">';
                template += '<button type="button" role="button" class="btn btn-default" ng-click="response(false)">Cancel</button>';
                template += '<button type="button" role="button" class="btn btn-primary" ng-click="response(true)">OK</button>';
                template += '</div>';
                template += '</div> ';
                var modalInstance = $uibModal.open({
                    template: template,
                    controller: 'angularUibConfirmCtrl',
                    windowClass: 'show',
                    size: 'sm',
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        message: function () {
                            return message;
                        },
                        buttons: function () {
                            return buttons;
                        }
                    }
                });
                modalInstance.result.then(function (response) {
                    q.resolve(response);
                }, function () {});
                return q.promise;
            }
        };
    }]);

angular.module('angular-uib-alert').controller("angularUibAlertCtrl", [
    '$scope',
    '$uibModalInstance',
    'message',
    function ($scope, $uibModalInstance, message) {
        $scope.message = message;
        $scope.close = function () {
            $uibModalInstance.close();
        };
    }]);

angular.module('angular-uib-alert').controller("angularUibConfirmCtrl", [
    '$scope',
    '$uibModalInstance',
    'message',
    'buttons',
    function ($scope, $uibModalInstance, message, buttons) {
        $scope.message = message;
        $scope.buttons = buttons;
        $scope.response = function (result) {
            $uibModalInstance.close(result);
        };
    }]);